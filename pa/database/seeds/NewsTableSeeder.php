<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class NewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('news')->truncate();

        DB::table('news')->insert([
            [
                'title'      => 'Laozi',
                'body'       => 'When there is no desire, all things are at peace.',
                'publish_at'   => '2015-06-17 18:08:03',
                'publish_flag' => '0',
                'delete_flag'  => '0',
                'created_at'   => '2015-06-17 18:07:01',
                'updated_at'   => '2015-06-17 18:08:02',
                'delete_at'    => '',
            ],
            [
                'title'      => 'Leonardo da Vinci',
                'body'       => 'Simplicity is the ultimate sophistication.',
                'publish_at'   => '2015-06-18 18:08:03',
                'publish_flag' => '0',
                'delete_flag'  => '0',
                'created_at'   => '2015-06-18 18:07:01',
                'updated_at'   => '2015-06-18 18:08:02',
                'delete_at'    => '',
            ],
            [
                'title'         => 'Cedric Bledsoe',
                'body'          => 'Simplicity is the essence of happiness.',
                'publish_at'   => '2015-06-19 18:08:03',
                'publish_flag' => '0',
                'delete_flag'  => '0',
                'created_at'   => '2015-06-19 18:07:01',
                'updated_at'   => '2015-06-19 18:08:02',
                'delete_at'    => '',
            ],
        ]);

        /*
・ID                                        :: id           :: int(11)
・件名                                      :: title        :: text
・本文                                      :: body         :: text
・公開日時[yyyy年mm月dd日00:00]             :: publish_at   :: timestamp
・公開フラグ[公開/非公開]                   :: publish_flag :: smallint(1)
・削除フラグ[○/×]                         :: delete_flag  :: smallint(1)
・登録日時[yyyy年mm月dd日00:00]             :: create_at    :: timestamp
・編集日時[yyyy年mm月dd日00:00]             :: update_at    :: timestamp
・削除日時[yyyy年mm月dd日00:00]             :: delete_at    :: timestamp
         */


    }
}
