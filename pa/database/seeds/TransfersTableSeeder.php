<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TransfersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transfers')->truncate();

        DB::table('transfers')->insert([
            [
                'member_id'          => '1',
                'flag'   => '0',
                'price'    => '100000',
                'created_at'    => '2015-06-20 12:05:01',
                'updated_at'    => '2015-06-20 12:05:02',
                'delete_at'    => '',
            ],
            [
                'member_id'          => '1',
                'flag'   => '0',
                'price'    => '50000',
                'created_at'    => '2015-06-21 12:05:01',
                'updated_at'    => '2015-06-21 12:05:02',
                'delete_at'    => '',
            ],
            [
                'member_id'          => '1',
                'flag'   => '0',
                'price'    => '200000',
                'created_at'    => '2015-06-22 12:05:01',
                'updated_at'    => '2015-06-22 12:05:02',
                'delete_at'    => '',
            ],
            [
                'member_id'          => '2',
                'flag'   => '1',
                'price'    => '50000',
                'created_at'    => '2015-06-22 12:05:01',
                'updated_at'    => '2015-06-22 12:05:02',
                'delete_at'    => '',
            ],
        ]);




    }
}
