<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call('UserTableSeeder');
        //$this->call('NewsTableSeeder');
        //$this->call('ProductsTableSeeder');
        //$this->call('MembersTableSeeder');
        $this->call('TransfersTableSeeder');
       // $this->call('AdministratorTableSeeder');



        Model::reguard();
    }
}
