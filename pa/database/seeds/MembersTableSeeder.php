<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MembersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('members')->truncate();

        DB::table('members')->insert([
            [

                'name_sei'        => '岩崎',
                'name_mei'        => '利久',
                'name_sei_kana'  => 'イワサキ',
                'name_mei_kana'  => 'トシヒサ',
                'email'           => 'iwasaki@able.jpn.com',
                'password'        => '$2y$10$SWrqvmRHsjbyywZDwOMsCOV14V2Kpsm7g9mQkYEXS0k5pnTf2lc9O',
                'member_flag'     => '0',
                'company_name'    => 'エイブルシステム株式会社',
                'zip_1'            => '170',
                'zip_2'            => '0005',
                'prefectures'     => '東京都',
                'municipality'    => '豊島区南大塚',
                'house_number'    => '1-60-20',
                'apartment'       => '天翔大塚駅前ビル202号室',
                'tel_1'            => '03',
                'tel_2'            => '6912',
                'tel_3'            => '2058',
                'bank'             => 'みずほ銀行',
                'branch'           => '大塚支店',
                'account_number'  => '3006625',
                'account_type'    => '0',
                'account_name'    => 'イワサキトシヒサ',
                'memo'             => '備考欄',
                'created_at'       => '2015-06-25 18:08:03',
                'updated_at'       => '2015-06-25 18:08:03',
                'delete_at'       => '',
                'remember_token' => '',
            ],

        ]);

        /*
・会員ID                        :: id             :: int(11)
・お名前（姓）                  :: name_sei       :: text
・お名前（名）                  :: name_mei       :: text
・お名前（セイ）                :: name_sei_kana  :: text
・お名前（メイ）                :: name_mei_kana  :: text
・メールアドレス                :: email          :: text
・パスワード                    :: password       :: text
・会員フラグ[個人・法人]        :: member_flag    :: smallint(1)
・会社名                        :: company_name   :: text
・郵便番号１                    :: zip_1          :: text
・郵便番号２                    :: zip_2          :: text
・都道府県                      :: prefectures    :: text
・市区町村名                    :: municipality   :: text
・番地                          :: house_number   :: text
・マンションアパート名          :: apartment      :: text
・電話番号１                    :: tel_1          :: text
・電話番号２                    :: tel_2          :: text
・電話番号３                    :: tel_3          :: text
・金融機関                      :: bank           :: text
・支店                          :: branch         :: text
・口座番号                      :: account_number :: text
・口座区分[普通/当座/貯蓄]      :: account_type   :: smallint(1)
・口座名義                      :: account_name   :: text
・メモ                          :: memo           :: text
・登録日時[yyyy年mm月dd日00:00] :: created_at      :: timestamp
・編集日時[yyyy年mm月dd日00:00] :: updated_at      :: timestamp
・削除日時[yyyy年mm月dd日00:00] :: delete_at      :: timestamp
・トークン
         */


    }
}
