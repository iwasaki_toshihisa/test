<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->truncate();

        DB::table('products')->insert([
            [
                'name'          => '商品名3',
                'category'   => '2',
                'publish_flag' => '1',
                'delete_flag'  => '0',
                'product_flag'   => '1',
                'code'   => 'CODE0003',
                'price'    => '19800',
                'detail'    => 'Simplicity is the ultimate sophistication.',
                'image_01'    => 'image_01_03.jpg',
                'image_02'    => 'image_02_03.jpg',
                'image_03'    => 'image_03_03.jpg',
                'created_at'    => '2015-06-24 12:05:01',
                'updated_at'    => '2015-06-24 12:05:02',
                'delete_at'    => '',
                'memo'    => '備考欄3',
            ],
            [
                'name'          => '商品名2',
                'category'   => '0',
                'publish_flag' => '1',
                'delete_flag'  => '1',
                'product_flag'   => '1',
                'code'   => 'CODE0002',
                'price'    => '6980',
                'detail'    => 'Simplicity is the ultimate sophistication.',
                'image_01'    => 'image_01_02.jpg',
                'image_02'    => 'image_02_02.jpg',
                'image_03'    => 'image_03_02.jpg',
                'created_at'    => '2015-06-24 10:05:01',
                'updated_at'    => '2015-06-24 10:05:02',
                'delete_at'    => '',
                'memo'    => '備考欄',
            ],
            [
                'name'          => '商品名1',
                'category'   => '1',
                'publish_flag' => '0',
                'delete_flag'  => '0',
                'product_flag'   => '0',
                'code'   => 'CODE0001',
                'price'    => '4980',
                'detail'    => 'Simplicity is the ultimate sophistication.',
                'image_01'    => 'image_01_01.jpg',
                'image_02'    => 'image_02_01.jpg',
                'image_03'    => 'image_03_01.jpg',
                'created_at'    => '2015-06-24 13:05:01',
                'updated_at'    => '2015-06-24 13:05:02',
                'delete_at'    => '',
                'memo'    => '備考欄',
            ],
        ]);

        /*
・ID                                        :: id           :: int(11)
・商品名                                    :: name         :: text
・商品カテゴリ                              :: category     :: int(11)
・公開フラグ[公開/非公開/シークレット]      :: publish_flag :: smallint(1)
・削除フラグ[○/×]                         :: delete_flag  :: smallint(1)
・商品フラグ[NEW/残りわずか/オススメ/限定]  :: product_flag :: smallint(1)
・商品コード                                :: code         :: text
・販売価格                                  :: price        :: decimal(10,0)
・商品詳細                                  :: detail       :: text

・商品画像メイン[xxxxxx.jpg]                :: image_01     :: text
・商品画像サブ１[xxxxxx.jpg]                :: image_02     :: text
・商品画像サブ２[xxxxxx.jpg]                :: image_03     :: text

・登録日時[yyyy年mm月dd日00:00]             :: create_at    :: timestamp
・編集日時[yyyy年mm月dd日00:00]             :: update_at    :: timestamp
・削除日時[yyyy年mm月dd日00:00]             :: delete_at    :: timestamp

・メモ                                      :: memo         :: text
         */


    }
}
