<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('members', function (Blueprint $table) {
            $table->increments('id');
            $table->string('member_id',128)->unique("member_");
            $table->string('name_sei');
            $table->string('name_mei');
            $table->string('name_sei_kana');
            $table->string('name_mei_kana');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('member_flag');
            $table->string('company_name');
            $table->string('zip_1');
            $table->string('zip_2');
            $table->mediumText('prefectures');
            $table->mediumText('municipality');
            $table->mediumText('house_number');
            $table->mediumText('apartment');
            $table->string('tel_1');
            $table->string('tel_2');
            $table->string('tel_3');
            $table->mediumText('bank');
            $table->mediumText('branch');
            $table->mediumText('account_number');
            $table->mediumText('account_type');
            $table->mediumText('account_name');
            $table->timestamps();  //created_atとupdate_atカラムの追加
            $table->timestamp('delete_at');
            $table->rememberToken();
            $table->longText('memo');
        });
        /*
・会員ID                        :: id             :: int(11)
・お名前（姓）                  :: name_sei       :: text
・お名前（名）                  :: name_mei       :: text
・お名前（セイ）                :: name_sei_kana  :: text
・お名前（メイ）                :: name_mei_kana  :: text
・メールアドレス                :: email          :: text
・パスワード                    :: password       :: text
・会員フラグ[個人・法人]        :: member_flag    :: smallint(1)
・会社名                        :: company_name   :: text
・郵便番号１                    :: zip_1          :: text
・郵便番号２                    :: zip_2          :: text
・都道府県                      :: prefectures    :: text
・市区町村名                    :: municipality   :: text
・番地                          :: house_number   :: text
・マンションアパート名          :: apartment      :: text
・電話番号１                    :: tel_1          :: text
・電話番号２                    :: tel_2          :: text
・電話番号３                    :: tel_3          :: text
・金融機関                      :: bank           :: text
・支店                          :: branch         :: text
・口座番号                      :: account_number :: text
・口座区分[普通/当座/貯蓄]      :: account_type   :: smallint(1)
・口座名義                      :: account_name   :: text
・メモ                          :: memo           :: text
・登録日時[yyyy年mm月dd日00:00] :: create_at      :: timestamp
・編集日時[yyyy年mm月dd日00:00] :: update_at      :: timestamp
・削除日時[yyyy年mm月dd日00:00] :: delete_at      :: timestamp
・トークン

         */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('members');
    }
}
