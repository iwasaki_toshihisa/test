<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('title');
            $table->longText('body');
            $table->timestamp('publish_at');
            $table->smallInteger('publish_flag');
            $table->smallInteger('delete_flag');
            $table->timestamps();  //created_atとupdate_atカラムの追加
            $table->timestamp('delete_at');
            /*
・ID                                        :: id           :: int(11)
・件名                                      :: title        :: text
・本文                                      :: body         :: text
・公開日時[yyyy年mm月dd日00:00]             :: publish_at   :: timestamp
・公開フラグ[公開/非公開]                   :: publish_flag :: smallint(1)
・削除フラグ[○/×]                         :: delete_flag  :: smallint(1)
・登録日時[yyyy年mm月dd日00:00]             :: create_at    :: timestamp
・編集日時[yyyy年mm月dd日00:00]             :: update_at    :: timestamp
・削除日時[yyyy年mm月dd日00:00]             :: delete_at    :: timestamp
             */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
