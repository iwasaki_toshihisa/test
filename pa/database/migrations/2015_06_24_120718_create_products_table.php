<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('name');
            $table->integer('category');
            $table->smallInteger('publish_flag');
            $table->smallInteger('delete_flag');
            $table->smallInteger('product_flag');
            $table->mediumText('code');
            $table->mediumText('price');
            $table->longText('detail');
            $table->binary('image_01');
            $table->binary('image_02');
            $table->binary('image_03');
            $table->timestamps();  //created_atとupdate_atカラムの追加
            $table->timestamp('delete_at');
            $table->rememberToken();
            $table->longText('memo');
            /*
・ID                                        :: id           :: int(11)
・商品名                                    :: name         :: text
・商品カテゴリ                              :: category     :: int(11)
・公開フラグ[公開/非公開/シークレット]      :: publish_flag :: smallint(1)
・削除フラグ[○/×]                         :: delete_flag  :: smallint(1)
・商品フラグ[NEW/残りわずか/オススメ/限定]  :: product_flag :: smallint(1)
・商品コード                                :: code         :: text
・販売価格                                  :: price        :: decimal(10,0)
・商品詳細                                  :: detail       :: text

・商品画像メイン[xxxxxx.jpg]                :: image_01     :: text
・商品画像サブ１[xxxxxx.jpg]                :: image_02     :: text
・商品画像サブ２[xxxxxx.jpg]                :: image_03     :: text

・登録日時[yyyy年mm月dd日00:00]             :: create_at    :: timestamp
・編集日時[yyyy年mm月dd日00:00]             :: update_at    :: timestamp
・削除日時[yyyy年mm月dd日00:00]             :: delete_at    :: timestamp

・メモ                                      :: memo         :: text
             */
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('news');
    }
}
