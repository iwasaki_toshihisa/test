<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    //

    protected $fillable = ['member_id', 'flag', 'price','balance'];

    public function member()
    {
        return $this->belongsTo('App\Member');
    }
}
