<?php namespace App;

use Illuminate\Auth\GenericUser;

class MemberUser extends GenericUser
{

public function getAuthIdentifier(){
return $this->attributes['member_id'];
}

}