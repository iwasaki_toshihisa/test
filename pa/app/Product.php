<?php

namespace App;


use Input;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name','publish_flag','product_flag','code','price','detail','image_01','image_02','image_03','memo'
    ];

    //base64にエンコードする関数を定義(image_01)
    public function getImage01Attribute($value)
    {
        //dd( $value);
        $base64 = base64_encode($value);
        $mime = 'image/jpg';
        return 'data:'.$mime.';base64,'.$base64;
    }

    public function setImage01Attribute($value)
    {
        $file = Input::file('image_01');
        $name = $file->getClientOriginalName();
        $tmp_name = $file->move('./img_tmp',$name);
        $contents= file_get_contents($tmp_name);
        $this->attributes['image_01'] =   $contents;
    }


    public function getImage02Attribute($value)
    {
        $base64 = base64_encode($value);
        $mime = 'image/jpg';
        return 'data:'.$mime.';base64,'.$base64;
    }
    public function setImage02Attribute($value)
    {
        $file = Input::file('image_02');
        $name = $file->getClientOriginalName();
        $tmp_name = $file->move('./img_tmp',$name);
        $contents= file_get_contents($tmp_name);
        $this->attributes['image_02'] =   $contents;
    }

    public function getImage03Attribute($value)
    {
        $base64 = base64_encode($value);
        $mime = 'image/jpg';
        return 'data:'.$mime.';base64,'.$base64;
    }
    public function setImage03Attribute($value)
    {
        $file = Input::file('image_03');
        $name = $file->getClientOriginalName();
        $tmp_name = $file->move('./img_tmp',$name);
        $contents= file_get_contents($tmp_name);
        $this->attributes['image_03'] =   $contents;
    }
}
