<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

//追加
use App\AdministratorProvider;
use App\MemberProvider;
use Illuminate\Auth\Guard;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $this->app['auth']->extend('admin',function($app){
            return new Guard(new AdministratorProvider,$app['session.store']);
        });
        //
        $this->app['auth']->extend('member',function($app){
            return new Guard(new MemberProvider,$app['session.store']);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
