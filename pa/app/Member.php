<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    //


    public function transfers()
    {
        return $this->hasMany('App\Transfer');
    }

    protected $table = 'members';

    protected $fillable = [
        'member_id',
        'name_sei',
        'name_mei',
        'name_sei_kana',
        'name_mei_kana',
        'email',
        'password',
        'member_flag',
        'company_name',
        'zip_1',
        'zip_2',
        'prefectures',
        'municipality',
        'house_number',
        'apartment',
        'tel_1',
        'tel_2',
        'tel_3',
        'bank',
        'branch',
        'account_number',
        'account_type',
        'account_name',
        'memo'
    ];
    protected $hidden = ['password', 'remember_token'];
}
