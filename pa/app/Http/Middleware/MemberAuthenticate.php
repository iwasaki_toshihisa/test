<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class MemberAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

//dd(Auth::driver('member')->check());
        if(Auth::driver('member')->check()){


            return view('member/index');

            //Do nothing.
        }else{
            //return 'go to member login page';
            return view('member/login');
        }


        return $next($request);
    }
}
