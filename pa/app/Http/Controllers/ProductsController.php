<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use DB;
use App\Product;
use App\Http\Requests;
use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //値を取得
        $s_name = Input::get('s_name');
        $s_publish_flag = Input::get('s_publish_flag');

        //$query = DB::table('products');
        $query = Product::latest('id');
        if(!empty($s_name)){
            //$query->where('id','like','%'.$uid.'%');
            $query->where('name','like','%'.$s_name.'%');
        }

        if($s_publish_flag != ""){

            //$query->where('email','like','%'.$email.'%');
            $query->where('publish_flag','=',$s_publish_flag);
        }

        //
        //$this->products->all();
        //$products = Product::all();
       // $products = Product::latest('id')->paginate(30);
        $products = $query->paginate(30);
        //$products =$query->latest('id')->paginate(30);
        //dd($products);
       // dd($products);
        return view('manager.products.index',compact('products'))->with('s_name',$s_name)->with('s_publish_flag',$s_publish_flag);
    }






    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('manager.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ProductRequest $request)
    {
        //
        Product::create($request->all());
        $d_text ='製品情報を登録しました。';
        \Session::flash('flash_message', $d_text);
        return redirect('/manager/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $product = Product::findOrNew($id);
        return View('manager.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $product = Product::findOrFail($id);
        return View('manager.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,ProductRequest $request)
    {
        //
        $product = Product::findOrFail($id);
        $product->update($request->all());
        $d_text ='ID:'.$id.'の製品情報を更新しました。';
        \Session::flash('flash_message', $d_text);
        return redirect(url('manager/products', [$product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $product = Product::findOrFail($id);
        $product->delete();
        $d_text ='ID:'.$id.'の製品情報を削除しました。';
        \Session::flash('flash_message', $d_text);
        return redirect('/manager/products');
    }
}
