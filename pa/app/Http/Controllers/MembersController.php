<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Member;
use App\News;
use Input;
use Auth;
use DB;
use App\Http\Requests;
use App\Http\Requests\MemberRequest;
use App\Http\Controllers\Controller;

class MembersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //

        $s_name_sei_kana = Input::get('s_name_sei_kana');
        $s_name_mei_kana = Input::get('s_name_mei_kana');
        $s_company_name = Input::get('s_company_name');

        $query = DB::table('members');

        if(!empty($s_name_sei_kana)){
            //$query->where('id','like','%'.$uid.'%');
            $query->where('name_sei_kana','like','%'.$s_name_sei_kana.'%');
        }

        if(!empty($s_name_mei_kana)){
            //$query->where('id','like','%'.$uid.'%');
            $query->where('name_mei_kana','like','%'.$s_name_mei_kana.'%');
        }

        if(!empty($s_company_name)){
            //$query->where('id','like','%'.$uid.'%');
            $query->where('company_name','like','%'.$s_company_name.'%');
        }

        $members = $query->latest('id')->paginate(30);
        //dd($news);
        return view('manager.members.index',compact('members'))->with('s_name_sei_kana',$s_name_sei_kana)->with('s_name_mei_kana',$s_name_mei_kana)->with('s_company_name',$s_company_name);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        $unique = uniqid('member_');

        return view('manager.members.create',compact('unique'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(MemberRequest $request)
    {
        Member::create($request->all());
        $d_text ='会員情報を登録しました。';
        \Session::flash('flash_message', $d_text);
        return redirect('/manager/members');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $member = Member::findOrNew($id);
        return view('manager.members.show', compact('member'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $member = Member::findOrFail($id);
        return view('manager.members.edit', compact('member'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, MemberRequest $request)
    {
        //
        $member = Member::findOrFail($id);
        $member->update($request->all());
        $d_text ='ID:'.$id.'の会員情報を更新しました。';
        \Session::flash('flash_message', $d_text);
        return redirect(url('/manager/members', [$member->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $member = Member::findOrFail($id);
        $member->delete();
        $d_text ='ID:'.$id.'の会員情報を削除しました。';
        \Session::flash('flash_message', $d_text);
        return redirect('/manager/members');
    }


}
