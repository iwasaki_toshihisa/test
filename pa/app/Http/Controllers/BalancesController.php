<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Input;
use App\Transfer;
use App\Member;
use App\Http\Requests\BalanceRequest;
use App\Http\Requests;

use App\Http\Controllers\Controller;

class BalancesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        //
        //値を取得
        $uid = Input::get('uid');
        $email = Input::get('email');
        $deposit='';
        //query
        $query = Member::query();


        //もしnameがあれば

        if(!empty($uid)){
            //$query->where('id','like','%'.$uid.'%');
            $query->where('id','=',$uid);
            //deposit
            $deposit = DB::table('transfers')->where('member_id','=',$uid)->sum('price');
            $balance = DB::table('transfers')->latest('created_at')->where('member_id','=',$uid)->take(5)->get();
            //SELECT SUM( `price` ) FROM `transfers` WHERE `member_id` = '1'
        }

        if(!empty($email)){
            //$query->where('email','like','%'.$email.'%');
            $query->where('email','=',$email);

            $tmp_id = DB::table('members')->where('email', $email)->pluck('id');
            $deposit = DB::table('transfers')->where('member_id','=',$tmp_id)->sum('price');
            $balance = DB::table('transfers')->latest('created_at')->where('member_id','=',$tmp_id)->take(5)->get();


        }
        if(!empty($uid) or !empty($email)){
            $member = $query->get();



            //dd($member);
            return view('manager.balances.index')->with('member',$member)->with('uid',$uid)->with('email',$email)->with('deposit',$deposit)->with('balance',$balance);

        }else{
            return view('manager.balances.index')->with('uid',$uid)->with('email',$email);
        }
    }





    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(BalanceRequest $request)
    {

        $request->balance = $request->balance - $request->price_out;
        //マイナス処理
        $request->price_out = '-'.$request->price_out;


 //       Request::input('price');
        //dd($request->price);
        //dd($request->all());
        //Transfer::create($request->all());
        Transfer::create([
            '_token' => $request->_token,
            'price'  => $request->price_out,
            'balance'  => $request->balance,
            'member_id' => $request->member_id,
            'flag'       => $request->flag,
        ]);
        $d_text ='出金が完了致しました。';
        \Session::flash('flash_message', $d_text);
        return redirect('/manager/balances');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
