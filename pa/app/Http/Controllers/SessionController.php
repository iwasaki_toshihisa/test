<?php

namespace App\Http\Controllers;


use Illuminate\Auth\Guard;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Requests\AutheticateRequest;

use App\Http\Controllers\Controller;



class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('member.login');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    //public function store(AutheticateRequest $request, Guard $auth)
    public function store(AutheticateRequest $request, Guard $auth)
    {
        // Auth::driver('member')->attempt(
        //dd($auth->driver('member')->attempt($request->only('email', 'password')));
       // dd(Auth::driver('member')->attempt($request->only('email', 'password')));

        //dd($auth->attempt($request->only('email', 'password')));
       // dd($request->input('email'));

        if(! Auth::driver('member')->attempt(['email'=>$request->input('email'),'password'=>$request->input('password')])){
            return redirect()->back();
        }else{
//            Auth::driver('member')->attempt(['email'=>'u2mo@docomo.ne.jp','password'=>'456456']);
           // Auth::driver('member')->attempt($request->only('email', 'password'));
            return redirect('/member');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy(Guard $auth)
    {
        //$auth->logout();
        Auth::driver('member')->logout();
        return redirect('/member');
    }
}
