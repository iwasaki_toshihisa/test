<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use Input;
use DB;
use App\Http\Requests;
use App\Http\Requests\NewsRequest;
use App\Http\Controllers\Controller;


class NewsController extends Controller
{

    public function __construct()
    {
       // $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //値を取得
        $s_title = Input::get('s_title');
        $s_publish_flag = Input::get('s_publish_flag');

        $query = DB::table('news');

        if(!empty($s_title)){
            //$query->where('id','like','%'.$uid.'%');
            $query->where('title','like','%'.$s_title.'%');
        }

        if($s_publish_flag != ""){

            //$query->where('email','like','%'.$email.'%');
            $query->where('publish_flag','=',$s_publish_flag);
        }




        //
        //$news = $this->news->all();
        //$news = News::all();
        //$news = News::orderBy('id', 'desc')->paginate(5);
        //$news = News::paginate();
        //$news = News::latest('id')->paginate(30);
        $news = $query->latest('id')->paginate(30);

        //dd($news);
        //dd($news);
        return view('manager.news.index',compact('news'))->with('s_title',$s_title)->with('s_publish_flag',$s_publish_flag);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('manager.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(NewsRequest $request)
    {
        /*
        $rules = [
        'title' => 'required|min:3',
        'body' => 'required',
        ];

        $this->validate($request, $rules);
        */
        News::create($request->all());
        //
        // ①フォームの入力値を取得
        //$inputs = \Request::all();

        // ②マスアサインメントを使って、記事をDBに作成
        //News::create($inputs);

        // ③記事一覧へリダイレクト
        $d_text ='お知らせ情報を登録しました。';
        \Session::flash('flash_message', $d_text);
        return redirect('/manager/news');
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        $news = News::findOrNew($id);

        return view('manager.news.show', compact('news'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        $news = News::findOrFail($id);
        return View('manager.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id,NewsRequest $request)
    {
        //
        $news = News::findOrFail($id);
        $news->update($request->all());
        $d_text ='ID:'.$id.'のお知らせを更新しました。';
        \Session::flash('flash_message', $d_text);
        return redirect(url('manager/news', [$news->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
        $news = News::findOrFail($id);
        $news->delete();
        $d_text ='ID:'.$id.'のお知らせを削除しました。';
        \Session::flash('flash_message', $d_text);
        return redirect('/manager/news');
    }
}
