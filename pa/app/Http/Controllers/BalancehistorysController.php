<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BalancehistorysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
        if(Auth::driver('member')->check())
        {
//            $news    = DB::table('news')->latest('id')->take(5)->get();

            //$transfer = DB::table('transfers')->latest('created_at')->where('member_id','=',Auth::driver('member')->user()->id)->get();
            $transfer = DB::table('transfers')->latest('created_at')->where('member_id','=',Auth::driver('member')->user()->id)->paginate(30);
            //dd($transfer);
            $deposit = DB::table('transfers')->where('member_id','=',Auth::driver('member')->user()->id)->sum('price');

            return view('member.bhistory')->with('deposit',$deposit)->with('transfer',$transfer);
        }else{
            return redirect('/member/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
