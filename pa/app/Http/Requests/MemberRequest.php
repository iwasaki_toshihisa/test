<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MemberRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name_sei_kana'  => 'required',
            'name_mei_kana'  => 'required',
            'name_sei'  => 'required',
            'name_mei'  => 'required',
            'email'     => 'required',
            'password'  => 'required',

        ];
    }
}
