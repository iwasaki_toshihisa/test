<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//管理画面TOP
Route::get('/manager', 'ManagersController@index');



//会員画面
/*
Route::group(['prefix'=>'member','middleware'=>'member'],function(){


    Route::get('/',function(){
        if(Auth::driver('member')->check())
        {
            return view('index');
        }else{
            return redirect('/login');
        }
    });


   // Route::get('/login','SessionController@create');
    //Route::get('/logout','SessionController@create');
    //Route::get('/login','SessionController@create');

   // Route::post('/login','SessionController@store');

});
*/
//会員画面TOP
Route::get('/member', 'MembershipsController@index');
Route::get('/member/thistory', 'TransferhistorysController@index');
Route::get('/member/bhistory', 'BalancehistorysController@index');
Route::get('/member/list', 'ProductlistsController@index');



Route::get('/member/login','SessionController@create');
Route::post('/member/login','SessionController@store');
Route::get('/member/logout','SessionController@destroy');
/*
Route::get('/member/login','SessionController@create');
Route::get('/member/logout','SessionController@create');
Route::get('/member/login','SessionController@create');
Route::post('/member/login','SessionController@store');
Route::post('/member/logout','SessionController@destroy');


Route::get('signin',['members'=>'SessionController@create', 'as'=>'signin']);
Route::get('signout',['members'=>'SessionController@destroy', 'as'=>'signout']);

Route::get('session',['members'=>'SessionController@create', 'as'=>'session']);
Route::post('session',['members'=>'SessionController@store', 'as'=>'session']);
Route::delete('session',['members'=>'SessionController@destroy', 'as'=>'session']);
*/

/*
Route::get('hoge',function(){

    //Auth::attempt(['email'=>'foo@test.com','password'=>'foo']);

    //認証（ログイン）
    Auth::driver('admin')->attempt(['email'=>'hoge@test.com','password'=>'hoge']);

    //情報取得
    $admin = Auth::driver('admin')->user();

    //ログインんできてないと$userが取得できないのでエラーになるはず
    return $admin->name;

});
Route::get('logout',function(){
    Auth::driver('admin')->logout();
});
*/
Route::get('test',function(){

    //Auth::attempt(['email'=>'foo@test.com','password'=>'foo']);

    //認証（ログイン）
    Auth::driver('member')->attempt(['email'=>'u2mo@docomo.ne.jp','password'=>'456456']);

    //dd(Auth::driver('member'));
    //情報取得
    $member = Auth::driver('member')->user();
//dd($member);
    //ログインんできてないと$userが取得できないのでエラーになるはず
    return $member->name_sei;

});
Route::get('logout',function(){
    Auth::driver('member')->logout();
});



//お知らせ管理
Route::get('/manager/news', 'NewsController@index');
Route::get('/manager/news/create', 'NewsController@create');
Route::get('/manager/news/{id}', 'NewsController@show');
Route::post('/manager/news', 'NewsController@store');
Route::get('/manager/news/{id}/edit','NewsController@edit');
Route::patch('/manager/news/{id}','NewsController@update');
Route::delete('/manager/news/{id}','NewsController@destroy');

//取扱製品管理
Route::get('/manager/products', 'ProductsController@index');
Route::get('/manager/products/create','ProductsController@create');
Route::get('/manager/products/{id}','ProductsController@show');
Route::post('/manager/products','ProductsController@store');
Route::get('/manager/products/{id}/edit','ProductsController@edit');
Route::patch('/manager/products/{id}','ProductsController@update');
Route::delete('/manager/products/{id}', 'ProductsController@destroy');



//会員管理
Route::get('/manager/members', 'MembersController@index');
Route::get('/manager/members/create', 'MembersController@create');
Route::get('/manager/members/{id}', 'MembersController@show');
Route::post('/manager/members', 'MembersController@store');
Route::get('/manager/members/{id}/edit','MembersController@edit');
Route::patch('/manager/members/{id}','MembersController@update');
Route::delete('/manager/members/{id}', 'MembersController@destroy');

//振込管理
Route::get('/manager/transfers', 'TransfersController@index');
Route::post('/manager/transfers', 'TransfersController@store');

//残高管理
Route::get('/manager/balances','BalancesController@index');
Route::post('/manager/balances','BalancesController@store');
