<?php namespace App;

use Illuminate\Auth\GenericUser;

class AdminUser extends GenericUser
{

    public function getAuthIdentifier(){
        return $this->attributes['administrator_id'];
    }

}