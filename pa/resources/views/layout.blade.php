{{-- ログインしている時だけ表示 --}}
@if (Auth::check())
<!DOCTYPE HTML>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <title>管理画面</title>
    <!-- CSSを追加 --><!-- ① 追加 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link href="{{ asset('css/common.css') }}" rel="stylesheet" type="text/css" >
</head>

<body>


@include('navbar')


<div class="container"><!-- ② 追加 -->
    {{-- フラッシュメッセージの表示 --}}
    @if (Session::has('flash_message'))
        <div class="alert alert-success">{{ Session::get('flash_message') }}</div>
    @endif


        @yield('content')



</div>

<!-- Scripts --><!-- ③ 追加 -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
    @else

    <SCRIPT type="text/javascript">

        setTimeout("redirect_url()", 0);
        function redirect_url(){
            location.href="    {{ url('/auth/login') }}";
        }

    </SCRIPT>
    //ページが切り替わります。自動的に切り替わらない場合には<a onclick="redirect_url()" href="#" &ht;コチラ</a&ht;をクリックしてください。
   @endif