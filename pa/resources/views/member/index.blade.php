{{-- ログインしている時だけ表示 --}}
{{--@if (Auth::check())--}}

@extends('member.layout')
@section('content')


<div class="deposit">
    ご利用可能金額：{{$deposit}} 円
</div>

<div class="news_box">
    <p class="news_title"><span class="glyphicon glyphicon-comment"></span> 更新情報</p>
    <dl class="news">
        @foreach($news as $news01)
            @if ($news01->publish_flag == 1)
        <dt>{{ $news01->title }}（{{ $news01->updated_at }}）</dt>
        <dd>{{	$news01->body}}</dd>
            @endif
        @endforeach
    </dl>

</div>







@stop
{{--@endif--}}