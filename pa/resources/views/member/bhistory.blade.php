{{-- ログインしている時だけ表示 --}}
{{--@if (Auth::check())--}}

@extends('member.layout')
@section('content')

    <div>
        <p><strong><a href="{{ url('/member') }}">ホーム</a></strong>&nbsp;＞&nbsp;<strong>残高履歴</strong></p>
    </div>

    <div class="deposit">
    ご利用可能金額：{{$deposit}} 円
</div>


<table class="table table-striped table-bordered table-condensed">
    <thead>
    <tr>
            <td colspan="4" class="transfer_title"><span class="glyphicon glyphicon-log-out"></span>  残高履歴</td>
        </tr>

<tr>
    <th class="w150">日付</th><th class="w150">項目</th><th>金額</th><th>残金</th>
    </tr>

    @foreach($transfer as $transfer01)

            <tr>
                <td>{{ $transfer01->created_at }}</td>
                <td  class="text-center">
                    @if ($transfer01->flag == 0)
                    ご入金
                    @else
                    ご注文
                    @endif
                    </td>
                <td class="text-right">{{$transfer01->price}} 円</td>

                <td class="text-right">
                    {{$transfer01->balance}}円




                    </td>
            </tr>

    @endforeach

    </table>

    {!! $transfer->setPath('')->appends(Input::query())->render() !!}


@stop
{{--@endif--}}