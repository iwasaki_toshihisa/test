<!---->
<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <!-- スマホやタブレットで表示した時のメニューボタン -->
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- ブランド表示 -->
            <a class="navbar-brand" href="{{ url('/member') }}">HOME</a>
        </div>

        <!-- メニュー -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <!-- 左寄せメニュー -->
            <ul class="nav navbar-nav">


                <li style="padding: 15px 15px;">代行店ID: {{ Auth::driver('member')->user()->id }}</li>
                <li style="padding: 15px 15px;">E-MAIL: {{ Auth::driver('member')->user()->email }}</li>

                <li><a href="{{ url('/member/thistory') }}">振込履歴</a></li>
                <li><a href="{{ url('/member/bhistory') }}">残高履歴</a></li>
                <li><a href="{{ url('/member/list') }}">取扱製品一覧</a></li>
            </ul>

            <!-- 右寄せメニュー -->
            <ul class="nav navbar-nav navbar-right">

                @if (!Auth::driver('member')->check())
                    {{-- ログインしていない時 --}}
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/auth/login') }}">Login</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ url('/auth/register') }}">Register</a></li>
                        </ul>
                    </li>
                @else
                    {{-- ログインしている時 --}}

                    <!-- ドロップダウンメニュー -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::driver('member')->user()->name_sei}} 様
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/member/logout') }}">Logout</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
