{{-- ログインしている時だけ表示 --}}
{{--@if (Auth::check())--}}

@extends('member.layout')
@section('content')

    <div>
        <p><strong><a href="{{ url('/member') }}">ホーム</a></strong>&nbsp;＞&nbsp;<strong>取扱製品一覧</strong></p>
    </div>

    <div class="deposit">
    ご利用可能金額：{{$deposit}} 円
</div>


    <table class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <td colspan="5" class="transfer_title"><span class="glyphicon glyphicon-th-list"></span>  取扱製品一覧</td>
        </tr>
        <tr>
            <th class="w80">ID</th>
            <th class="w150">商品画像</th>
            <th class="w150">商品名</th>
            <th class="w120">販売価格</th>
            <th >商品詳細</th>





        </tr>
        </thead>
        <tbody>
        @foreach($products as $products01)
            @if ($products01->publish_flag == 1)
            <tr>
                <td>{{$products01->id}}</td>
                <td>
                    @if ($products01->image_01 != "data:image/jpg;base64,")
                        <?php
                        $base64 = base64_encode($products01->image_01);
                        $mime = 'image/jpg';
                        $products01->image_01= 'data:'.$mime.';base64,'.$base64;
                        ?>

                        <img src="{{$products01->image_01}}" width="130" height="130"><br>
                    @endif</td>


                <td>{{$products01->name}}</td>


                <td> @if ($products01->price != "")
                        {{$products01->price}}円
                    @endif
                </td>
                <td>{{$products01->detail}}</td>



            </tr>
            @endif
        @endforeach
        </tbody>
    </table>

    {!! $products->setPath('')->appends(Input::query())->render() !!}
    <br>




@stop
{{--@endif--}}