@extends('layout')
@section('content')

    <div>
        <p><strong>会員管理</strong>&nbsp;＞&nbsp;<strong>ID:{{ $member->id }}の会員情報詳細</strong></p>
    </div>

    <table class="table table-striped table-bordered table-condensed">
    <thead>
    <tr><th>ID</th><td>{{$member->id}} </td></tr>
    <tr><th>お名前</th>
        <td>{{$member->name_sei_kana}} {{$member->name_mei_kana}}<br>
            {{$member->name_sei}} {{$member->name_mei}} </td></tr>

    <tr><th>メールアドレス</th>
        <td>{{$member->email}}</td>
    </tr>
    <tr><th>パスワード</th>
        <td>{{$member->password}}</td>
    </tr>

    <tr><th>会員フラグ</th><td>
            @if ($member->member_flag == 0)
                個人
            @else
                法人
            @endif
        </td></tr>

    <tr><th>会社名</th>
        <td>{{$member->company_name}}</td>
    </tr>

    <tr><th>住所</th>
        <td>〒{{$member->zip_1}}-{{$member->zip_2}}<br>
        {{$member->prefectures}}{{$member->municipality}}{{$member->house_number}}<br>
            {{$member->apartment}}
        </td>
    </tr>
    <tr><th>電話番号</th>
        <td>{{$member->tel_1}}-{{$member->tel_2}}-{{$member->tel_3}}</td>
    </tr>

    <tr><th>口座情報</th>
        <td>{{$member->bank}}<br>
            {{$member->branch}}<br>
            {{$member->account_number}}<br>
            @if ($member->account_type == 0)
                普通
            @elseif($member->account_type == 1)
                当座
            @else
                貯蓄
            @endif
            <br>
            {{$member->account_name}}
        </td>
    </tr>
    <tr>
        <th>メモ</th>
        <td>{{$member->memo}}

        </td>
    </tr>

    <tr><th>登録日時</th><td>{{$member->created_at}}</td></tr>
    <tr><th>編集日時</th><td>{{$member->updated_at}}</td></tr>
     </thead>
</table>
    <a href="{{ url('/manager/members') }}" class="btn btn-default btn-xs">一覧へ</a>
@stop