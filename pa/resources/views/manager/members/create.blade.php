@extends('layout')
@section('content')

<div>
<p><strong>会員管理</strong>&nbsp;＞&nbsp;<strong>会員新規登録</strong></p>
</div>

{{-- エラーの表示を追加 --}}
@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

{!! Form::open(['url' => '/manager/members']) !!}
<div class="form-group">
    {!! Form::label('name_sei','お名前（カナ）：') !!}<br>
    {!! Form::text('name_sei_kana', null, ['class' => 'form-control,col-xs-2','placeholder'=>'セイ']) !!}
    {!! Form::text('name_mei_kana', null, ['class' => 'form-control,col-xs-2','placeholder'=>'メイ']) !!}
</div>
<div class="form-group">
    {!! Form::label('name_sei','お名前（姓名）（必須）：') !!}<br>
    {!! Form::text('name_sei', null, ['class' => 'form-control,col-xs-2','placeholder'=>'姓']) !!}
    {!! Form::text('name_mei', null, ['class' => 'form-control,col-xs-2','placeholder'=>'名']) !!}
</div>

<div class="form-group">
    {!! Form::label('email','メールアドレス（必須）：') !!}<br>
    {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'example@gmail.com']) !!}
</div>

<div class="form-group">
    {!! Form::label('password','パスワード（必須）：') !!}<br>
    {!! Form::text('password', null, ['class' => 'form-control','placeholder'=>'パスワード[英数字6文字以上]']) !!}
</div>

<div class="form-group">
    {!! Form::label('member_flag', '公開フラグ：') !!}<br>
    {!! Form::select('member_flag', array('0' => "個人", '1' => "法人")) !!}
</div>

<div class="form-group">
    {!! Form::label('company_name','会社名：') !!}<br>
    {!! Form::text('company_name', null, ['class' => 'form-control','placeholder'=>'会社名']) !!}
</div>

<div class="form-group">
    {!! Form::label('zip_1','郵便番号：') !!}<br>
    〒{!! Form::text('zip_1', null, ['class' => 'form-control,col-xs-2','placeholder'=>'xxx']) !!}-
    {!! Form::text('zip_2', null, ['class' => 'form-control,col-xs-2','placeholder'=>'xxxx']) !!}
</div>
<div class="form-group">
    {!! Form::label('prefectures','都道府県：') !!}<br>
    {!! Form::text('prefectures', null, ['class' => 'form-control','placeholder'=>'都道府県']) !!}
</div>
<div class="form-group">
    {!! Form::label('municipality','市区町村名：') !!}<br>
    {!! Form::text('municipality', null, ['class' => 'form-control','placeholder'=>'市区町村名']) !!}
</div>
<div class="form-group">
    {!! Form::label('house_number','番地：') !!}<br>
    {!! Form::text('house_number', null, ['class' => 'form-control','placeholder'=>'番地']) !!}
</div>
<div class="form-group">
    {!! Form::label('apartment','アパート・マンション名：') !!}<br>
    {!! Form::text('apartment', null, ['class' => 'form-control','placeholder'=>'アパート・マンション名']) !!}
</div>

<div class="form-group">
    {!! Form::label('tel_1','電話番号') !!}<br>
    {!! Form::text('tel_1', null, ['class' => 'form-control,col-xs-2','placeholder'=>'xxxx']) !!}-
    {!! Form::text('tel_2', null, ['class' => 'form-control,col-xs-2','placeholder'=>'xxxx']) !!}-
    {!! Form::text('tel_3', null, ['class' => 'form-control,col-xs-2','placeholder'=>'xxxx']) !!}
</div>


<h4>口座情報</h4>
<div class="form-group">
    {!! Form::label('bank','金融機関：') !!}<br>
    {!! Form::text('bank', null, ['class' => 'form-control','placeholder'=>'○○銀行']) !!}
</div>

<div class="form-group">
    {!! Form::label('branch','支店：') !!}<br>
    {!! Form::text('branch', null, ['class' => 'form-control','placeholder'=>'○○支店']) !!}
</div>

<div class="form-group">
    {!! Form::label('account_number','口座番号：') !!}<br>
    {!! Form::text('account_number', null, ['class' => 'form-control','placeholder'=>'xxxxxx']) !!}
</div>

<div class="form-group">
    {!! Form::label('account_type', '口座区分：') !!}<br>
    {!! Form::select('account_type', array('0' => "普通", '1' => "当座",'2' =>'貯蓄')) !!}
</div>

<div class="form-group">
    {!! Form::label('account_name', '口座名義：') !!}<br>
    {!! Form::text('account_name', null, ['class' => 'form-control','placeholder'=>'ヤマダ タロウ']) !!}
</div>

<div class="form-group">
    {!! Form::submit('会員情報を登録する', ['class' => 'btn btn-primary form-control']) !!}
</div>




{!! Form::hidden('member_id',$unique) !!}


{!! Form::close() !!}


@stop