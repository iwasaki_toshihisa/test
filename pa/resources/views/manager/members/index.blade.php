@extends('layout')
@section('content')


<div>
    <p><strong>会員管理</strong>&nbsp;＞&nbsp;<strong>会員一覧</strong></p>
</div>

<div>
    <p><a href="{{ url('/manager/members/create') }}" class="btn btn-primary">新規登録</a></p>
</div>

<div class="search">
    <form method="get" action="./members">
        お名前(セイ)：<input type="text" name="s_name_sei_kana" value="{{ $s_name_sei_kana }}">
        (メイ)：<input type="text" name="s_name_mei_kana" value="{{ $s_name_mei_kana }}">
        会社名：<input type="text" name="s_company_name" value="{{ $s_company_name }}">

        <input type="submit" value="表示">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    </form>
</div>


<table class="table table-striped table-bordered table-condensed">
    <thead>
    <tr>
        <th>ID</th>
        <th>お名前</th>
        <th>会員フラグ</th>
        <th>会社名</th>
        <th>住所</th>
        <th>電話</th>
        <th>口座情報</th>
        <th>登録日時</th>

        <th>詳細</th>
        <th>編集</th>
        <th>削除</th>
    </tr>
    </thead>
    <tbody>
    @foreach($members as $members01)

        <tr>
            <td>{{$members01->id}}</td>
            <td><small>{{$members01->name_sei_kana}} {{$members01->name_mei_kana}}</small>
                <br>{{$members01->name_sei}} {{$members01->name_mei}}</td>

            <td>
                @if ($members01->member_flag == 0)
                    個人
                @else
                    法人
                @endif
            </td>
            <td>{{$members01->company_name}}</td>
            <td>
                〒{{$members01->zip_1}}-{{$members01->zip_2}}<br>
                {{$members01->prefectures}}
                {{$members01->municipality}}{{$members01->house_number}}<br>
                {{$members01->apartment}}

            </td>
            <td>{{$members01->tel_1}}-{{$members01->tel_2}}-{{$members01->tel_3}}</td>
            <td>
                {{$members01->bank}}<br>
                {{$members01->branch}}<br>
                {{$members01->account_number}} <br>
                {{$members01->account_name}}

            </td>
            <td>{{$members01->created_at}}</td>

            <td><a href="{{ url('/manager/members', $members01->id) }}" class="btn btn-default btn-xs">詳細</a></td>
            <td><a href="{{ url('/manager/members', $members01->id ) }}/edit" class="btn btn-default btn-xs">編集</a></td>
            <td>{!! Form::open(['method' => 'DELETE', 'url' => ['/manager/members', $members01->id]]) !!}
                {!! Form::submit('削除', ['class' => 'btn btn-danger btn-xs']) !!}
                {!! Form::close() !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

{!! $members->setPath('')->appends(Input::query())->render() !!}
<br>

@stop