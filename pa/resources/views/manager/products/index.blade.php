@extends('layout')
@section('content')


<div>
    <p><strong>取扱製品管理</strong>&nbsp;＞&nbsp;<strong>製品一覧</strong></p>
</div>

<div>
    <p><a href="{{ url('/manager/products/create') }}" class="btn btn-primary">新規登録</a></p>
</div>

<div class="search">
    <form method="get" action="./products">
        商品名：<input type="text" name="s_name" value="{{ $s_name }}">
        公開フラグ：<select name="s_publish_flag">
            <option value="" @if($s_publish_flag == "" ) selected @endif >　</option>
            <option value="0" @if($s_publish_flag == "0" ) selected @endif>非公開</option>
            <option value="1" @if($s_publish_flag == "1" ) selected @endif>公開</option>
        </select>

        <input type="submit" value="表示">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    </form>
</div>

<table class="table table-striped table-bordered table-condensed">
    <thead>
    <tr>
        <th>ID</th>
        <th>商品名</th>

        <th>公開フラグ</th>

        <th>商品コード</th>
        <th>販売価格</th>

        <th>商品画像メイン</th>

        <th>登録日時</th>
        <th>編集日時</th>
        <th>詳細</th>
        <th>編集</th>
        <th>削除</th>
    </tr>
    </thead>
    <tbody>

    @foreach($products as $products01)

        <tr>
            <td>{{$products01->id}}</td>
            <td>{{$products01->name}}</td>

            <td>
                @if ($products01->publish_flag == 0)
                    非公開
                @else
                    公開
                @endif
            </td>

            <td>{{$products01->code}}</td>
            <td> @if ($products01->price != "")
                    {{$products01->price}}円
                @endif
            </td>
            <td>



                @if ($products01->image_01 != "data:image/jpg;base64,")
                    <img src="{{$products01->image_01}}" width="130" height="130"><br>
                @endif


            </td>

            <td>{{$products01->created_at}}</td>
            <td>{{$products01->updated_at}}</td>
            <td><a href="{{ url('/manager/products', $products01->id) }}" class="btn btn-default btn-xs">詳細</a></td>
            <td><a href="{{ url('/manager/products', $products01->id ) }}/edit" class="btn btn-default btn-xs">編集</a></td>
            <td>{!! Form::open(['method' => 'DELETE', 'url' => ['/manager/products', $products01->id]]) !!}
                {!! Form::submit('削除', ['class' => 'btn btn-danger btn-xs']) !!}
                {!! Form::close() !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

{!! $products->setPath('')->appends(Input::query())->render() !!}
<br>
@stop
