@extends('layout')
@section('content')

    <div>
        <p><strong>取扱製品管理</strong>&nbsp;＞&nbsp;<strong>ID:{{{ $product->id }}}の製品情報詳細</strong></p>
    </div>

    <table class="table table-striped table-bordered table-condensed">
    <thead>
    <tr><th>ID</th><td>{{{ $product->id }}}</td></tr>
    <tr><th>商品名</th><td>{{{ $product->name }}}</td></tr>

    <tr><th>公開フラグ</th><td>
            @if ($product->publish_flag == 0)
                非公開
            @else
                公開
            @endif
        </td></tr>
    <tr><th>商品コード</th><td>{{$product->code}}</td></tr>
    <tr><th>販売価格</th><td>{{$product->price}}</td></tr>
    <tr><th>商品詳細</th><td>{{$product->detail}}</td></tr>
    <tr><th>商品画像メイン</th>
        <td>
            @if ($product->image_01 != "data:image/jpg;base64,")
            <img src="{{$product->image_01}}">
            @endif
        </td></tr>
    <!--
    <tr><th>商品画像サブ１</th>
        <td>
            @if ($product->image_02 != "data:image/jpg;base64,")
                <img src="{{$product->image_02}}">
            @endif
        </td></tr>
    <tr><th>商品画像サブ２</th>
        <td>
            @if ($product->image_03 != "data:image/jpg;base64,")
                <img src="{{$product->image_03}}">
            @endif
        </td></tr>
        -->
    <tr><th>登録日時</th><td>{{{ $product->created_at }}}</td></tr>
    <tr><th>編集日時</th><td>{{{ $product->updated_at }}}</td></tr>
    <tr><th>メモ</th><td>{{$product->memo}}</td></tr>
    </thead>
</table>
    <a href="{{ url('/manager/products') }}" class="btn btn-default btn-xs">一覧へ</a>
@stop