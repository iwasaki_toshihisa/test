@extends('layout')
@section('content')

<div>
<p><strong>取扱製品管理</strong>&nbsp;＞&nbsp;<strong>製品新規登録</strong></p>
</div>


{{-- エラーの表示を追加 --}}
@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

{!! Form::model($product, ['method' => 'PATCH','url' => ['/manager/products', $product->id],'files' => true]) !!}
<div class="form-group">
    {!! Form::label('name', '商品名：') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('publish_flag', '公開フラグ：') !!}
    {!! Form::select('publish_flag', array('0' => "非公開", '1' => "公開")) !!}
</div>

<div class="form-group">
    {!! Form::label('code', '商品コード：') !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('price', '販売価格：') !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('detail', '商品詳細：') !!}
    {!! Form::textarea('detail', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    @if ($product->image_01 != "data:image/jpg;base64,")
        <img src="{{$product->image_01}}" width="130" height="130"><br>
    @endif

    {!! Form::label('image_01','商品画像メイン') !!}
    {!! Form::file('image_01', null, ['class' => 'form-control']) !!}
</div>

<!--
<div class="form-group">
    @if ($product->image_02 !="data:image/jpg;base64,")
    <img src="{{$product->image_02}}" width="130" height="130"><br>
    @endif
    {!! Form::label('image_02','商品画像サブ１') !!}
    {!! Form::file('image_02', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    @if ($product->image_03 !="data:image/jpg;base64,")
        <img src="{{$product->image_03}}" width="130" height="130"><br>
    @endif
    {!! Form::label('image_03','商品画像サブ２') !!}
    {!! Form::file('image_03', null, ['class' => 'form-control']) !!}
</div>
-->
<div class="form-group">
    {!! Form::label('memo', 'メモ：') !!}
    {!! Form::textarea('memo', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group">
    {!! Form::submit('製品を登録する', ['class' => 'btn btn-primary form-control']) !!}
</div>
{!! Form::close() !!}

@stop