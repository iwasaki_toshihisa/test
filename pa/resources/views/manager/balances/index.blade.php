@extends('layout')
@section('content')


    <div>
        <p><strong>残高管理</strong>&nbsp;＞&nbsp;<strong>出金</strong></p>
    </div>

    {{-- エラーの表示を追加 --}}
    @if ($errors->any())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <script type="text/javascript">
        <!--

        function check(){

            if(window.confirm('出金処理を行います。宜しいですか？')){ // 確認ダイアログを表示
                return true; // 「OK」時は実行
            }
            else{ // 「キャンセル」時の処理
                window.alert('キャンセルされました'); // 警告ダイアログを表示
                return false; // 中止
            }
        }

        // -->
    </script>
    <div class="search">
    <form method="get" action="./balances">
        会員ID：<input type="text" name="uid" value="{{ $uid }}">
        メールアドレス：<input type="text" name="email" value="{{ $email }}">
        <input type="submit" value="表示">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
    </form>

    </div>
    <!--
    {!! Form::open(['url' => '/manager/transfers', 'method' => 'get']) !!}
    <div class="form-group">
        {!! Form::label('uid', '会員ID：') !!}
        {!! Form::text('uid', null, ['class' => 'form-control']) !!}
        {!! Form::label('email', 'EMAIL：') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}

        {!! Form::submit('表示', ['class' => 'btn btn-primary form-control']) !!}
    </div>

    {!! Form::close() !!}
-->
    @if(!empty($balance))
    <table class="table table-striped table-bordered table-condensed">
        <thead>
        <tr>
            <td colspan="4" class="transfer_title"><span class="glyphicon glyphicon-log-out"></span>  残高履歴（直近:５件）</td>
        </tr>

        <tr>
            <th class="w150">日付</th><th class="w150">項目</th><th>金額</th><th>残金</th>
        </tr>

        @foreach($balance as $balance01)

            <tr>
                <td>{{ $balance01->created_at }}</td>
                <td  class="text-center">
                    @if ($balance01->flag == 0)
                        ご入金
                    @else
                        ご注文
                    @endif
                </td>
                <td class="text-right">{{$balance01->price}} 円</td>

                <td class="text-right">
                    {{$balance01->balance}}円
                </td>
            </tr>

        @endforeach

    </table>
@endif

    @if(!empty($member))
        @foreach($member as $member01)
            {!! Form::open(['url' => '/manager/balances', 'method' => 'post','onSubmit'=>"return check()"]) !!}
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr><th>会員ID</th><td> {{$member01->id}}</td></tr>
                <tr><th>お名前</th><td> {{$member01->name_sei}} {{$member01->name_mei}}</td></tr>
                <tr><th>所持金額</th><td>{{$deposit}} 円</td></tr>
                <tr class="danger"><th>出金額（必須）</th><td> - {!! Form::text('price_out', null, ['class' => 'form-control,col-xs-2']) !!} 円</td></tr>
                </thead>
            </table>
            {!! Form::hidden('member_id', $member01->id) !!}
            {!! Form::hidden('balance', $deposit) !!}
            {!! Form::hidden('flag', '1') !!}
            {!! Form::submit('出金する', ['class' => 'btn btn-primary form-control']) !!}
        @endforeach
    @endif


@stop