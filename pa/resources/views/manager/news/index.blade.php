@extends('layout')
@section('content')
<div>
    <p><strong>お知らせ管理</strong>&nbsp;＞&nbsp;<strong>お知らせ一覧</strong></p>
</div>

<div>
    <p><a href="{{ url('/manager/news/create') }}" class="btn btn-primary">新規登録</a></p>
</div>

<div class="search">
<form method="get" action="./news">
    件名：<input type="text" name="s_title" value="{{ $s_title }}">
    公開フラグ：<select name="s_publish_flag">
        <option value="" @if($s_publish_flag == "" ) selected @endif >　</option>
        <option value="0" @if($s_publish_flag == "0" ) selected @endif>非公開</option>
        <option value="1" @if($s_publish_flag == "1" ) selected @endif>公開</option>
    </select>

    <input type="submit" value="表示">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
</form>
</div>

<table class="table table-striped table-bordered table-condensed">
    <thead>
    <tr>
        <th>ID</th>
        <th>件名</th>
        <!--<th>本文</th>-->
        <!--<th>公開日時</th>-->
        <th>公開フラグ</th>
        <!--<th>削除フラグ</th>-->
        <th>登録日時</th>
        <th>編集日時</th>
        <!--<th>削除日時</th>-->
        <th>詳細</th>
        <th>編集</th>
        <th>削除</th>
    </tr>
    </thead>
    <tbody>
    @foreach($news as $news01)

        <tr>
            <td>{{{ $news01->id }}}</td>
            <td>{{{ $news01->title }}}</td>
            <!--<td>{{{ $news01->body }}}</td>-->
            <!--<td>{{{ $news01->publish_at }}}</td>-->
            <td>
                @if ($news01->publish_flag == 0)
               非公開
                @else
                   公開
                @endif


            </td>
            <!--<td>{{{ $news01->delete_flag }}}</td>-->
            <td>{{{ $news01->created_at }}}</td>
            <td>{{{ $news01->updated_at }}}</td>
            <!--<td>{{{ $news01->delete_at }}}</td>-->
            <td><a href="{{ url('/manager/news', $news01->id) }}" class="btn btn-default btn-xs">詳細</a>
            </td>
            <td><a href="{{ url('/manager/news', $news01->id ) }}/edit" class="btn btn-default btn-xs">編集</a></td>
            <td>{!! Form::open(['method' => 'DELETE', 'url' => ['/manager/news', $news01->id]]) !!}
                {!! Form::submit('削除', ['class' => 'btn btn-danger btn-xs']) !!}
                {!! Form::close() !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>

{!! $news->setPath('')->appends(Input::query())->render() !!}
<br>
@stop
