@extends('layout')
@section('content')

    <div>
        <p><strong>お知らせ管理</strong>&nbsp;＞&nbsp;<strong>ID:{{{ $news->id }}}のお知らせ詳細</strong></p>
    </div>

    <table class="table table-striped table-bordered table-condensed">
    <thead>
    <tr><th>ID</th><td>{{{ $news->id }}}</td></tr>
    <tr><th>件名</th><td>{{{ $news->title }}}</td></tr>
    <tr><th>本文</th><td>{{{ $news->body }}}</td></tr>
    <!--<tr><th>公開日時</th><td>{{{ $news->publish_at }}}</td></tr>-->
    <tr><th>公開フラグ</th><td>
            @if ($news->publish_flag == 0)
                非公開
            @else
                公開
            @endif
        </td></tr>
    <!--<tr><th>削除フラグ</th><td>{{{ $news->delete_flag }}}</td></tr>-->
    <tr><th>登録日時</th><td>{{{ $news->created_at }}}</td></tr>
    <tr><th>編集日時</th><td>{{{ $news->updated_at }}}</td></tr>
    <!--<tr><th>削除日時</th><td>{{{ $news->delete_at }}}</td></tr>-->
    </thead>
</table>
    <a href="{{ url('/manager/news') }}" class="btn btn-default btn-xs">一覧へ</a>
@stop