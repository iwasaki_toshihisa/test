@extends('layout')
@section('content')

<div>
<p><strong>お知らせ管理</strong>&nbsp;＞&nbsp;<strong>お知らせ新規登録</strong></p>
</div>


{{-- エラーの表示を追加 --}}
@if ($errors->any())
    <ul class="alert alert-danger">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif

{!! Form::open(['url' => '/manager/news']) !!}
<div class="form-group">
    {!! Form::label('title', '件名(必須)：') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('body', '本文(必須)：') !!}
    {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('publish_flag', '公開フラグ：') !!}
    {!! Form::select('publish_flag', array('0' => "非公開", '1' => "公開")) !!}
</div>
<!--
<div class="form-group">
    {!! Form::label('published_at', 'Publish On:') !!}
    {!! Form::input('date', 'published_at', date('Y-m-d'), ['class' => 'form-control']) !!}
</div>
-->
<div class="form-group">
    {!! Form::submit('お知らせを登録する', ['class' => 'btn btn-primary form-control']) !!}
</div>
{!! Form::close() !!}

@stop